import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Task } from '../../models/task.model';
import { Store } from '@ngrx/store';
import * as data from '../../actions/data';
import * as fromDashboard from '../../reducers';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit {

  tasks$: Observable<Task[]>;

  constructor(
    private store: Store<fromDashboard.State>
  ) {
    this.tasks$ = store.select(fromDashboard.getTasks);
  }

  ngOnInit() {
    this.store.dispatch(new data.GetTasks());
  }

}
