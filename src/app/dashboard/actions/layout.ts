import { Action } from '@ngrx/store';

import { ToolbarOptions } from '../models/toolbar.model';

export const SET_TOOLBAR_OPTIONS = '[Dashboard / Layout] Set toolbar options';

export class SetToolbarOptions implements Action {
  readonly type = SET_TOOLBAR_OPTIONS;

  constructor(public payload: ToolbarOptions) { }
}

export type Actions
  = SetToolbarOptions;
