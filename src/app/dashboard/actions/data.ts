import { Action } from '@ngrx/store';
import { Task } from '../models/task.model';

export const GET_TASKS = '[Dashboard / Data] Get tasks';
export const GET_TASKS_SUCCESS = '[Dashboard / Data] Get tasks success';
export const GET_TASKS_FAIL = '[Dashboard / Data] Get tasks fail';

export const ADD_TASK = '[Dashboard / Data] Add task';
export const ADD_TASK_SUCCESS = '[Dashboard / Data] Add task success';
export const ADD_TASK_FAIL = '[Dashboard / Data] Add task fail';

export class GetTasks implements Action {
  readonly type = GET_TASKS;
}

export class GetTasksSuccess implements Action {
  readonly type = GET_TASKS_SUCCESS;

  constructor(public payload: Task[]) { }
}

export class GetTasksFail implements Action {
  readonly type = GET_TASKS_FAIL;
}

export class AddTask implements Action {
  readonly type = ADD_TASK;

  constructor(public payload: Task) { }
}

export class AddTaskSuccess implements Action {
  readonly type = ADD_TASK_SUCCESS;
}

export class AddTaskFail implements Action {
  readonly type = ADD_TASK_FAIL;
}

export type Actions
  = GetTasks
  | GetTasksSuccess
  | GetTasksFail
  | AddTask
  | AddTaskSuccess
  | AddTaskFail;
