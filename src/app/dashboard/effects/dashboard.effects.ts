import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs/observable/of';

import * as DashboardActions from '../actions/data';

@Injectable()
export class DashboardEffects {

  private url = {
    addTask: () => {
      return `http://localhost:3000/add-task`;
    },
    getTasks: () => {
      return `http://localhost:3000/tasks`;
    }
  };

  constructor(
    private http: HttpClient,
    private actions$: Actions
  ) {
    /* -- */
  }

  @Effect() getTasks$: Observable<Action> = this.actions$.ofType(DashboardActions.GET_TASKS)
    .mergeMap((action: DashboardActions.GetTasks) => this.http.get(this.url.getTasks()))
    .map(data => ({type: DashboardActions.GET_TASKS_SUCCESS, payload: data}))
    .catch(() => of({type: DashboardActions.GET_TASKS_FAIL}));

  @Effect() addTask$: Observable<Action> = this.actions$.ofType(DashboardActions.ADD_TASK)
    .mergeMap((action: DashboardActions.AddTask) => this.http.post(this.url.addTask(), action.payload))
    .map(data => ({type: DashboardActions.ADD_TASK_SUCCESS, payload: data}))
    .catch(() => of({type: DashboardActions.ADD_TASK_FAIL}));

  /* TODO: Add effect that will handle ADD_TASK_SUCCESS action. To do that add navigation actions. */
  /* TODO: More info here: https://github.com/ngrx/platform/blob/master/docs/router-store/api.md#navigation-actions */
  /* TODO: And here: https://github.com/ngrx/platform/blob/master/docs/router-store/README.md */

}
