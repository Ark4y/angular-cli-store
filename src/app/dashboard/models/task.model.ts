export interface Task {
    title: string;
    subtitle: string;
    picture?: string;
    content: string;
}
