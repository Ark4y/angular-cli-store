export interface ToolbarButton {
  title: string;
  route: string;
}

export interface ToolbarOptions {
  buttons: ToolbarButton[];
}
