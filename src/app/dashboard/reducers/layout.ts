import * as data from '../actions/layout';
import { ToolbarOptions } from '../models/toolbar.model';

export interface State {
  toolbarOptions: ToolbarOptions;
}

export const initialState: State = {
  toolbarOptions: {
    buttons: []
  }
};

export function reducer(state = initialState, action: data.Actions): State {
  switch (action.type) {
    case data.SET_TOOLBAR_OPTIONS: {
      return {
        ...state,
        toolbarOptions: action.payload
      };
    }

    default: {
      return state;
    }
  }
}

export const getToolbarOptions = (state: State) => state.toolbarOptions;
