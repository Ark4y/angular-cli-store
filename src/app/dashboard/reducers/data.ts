import { Task } from '../models/task.model';
import * as data from '../actions/data';

export interface State {
  tasks: Task[];
}

export const initialState: State = {
  tasks: []
};

export function reducer(state = initialState, action: data.Actions): State {
  switch (action.type) {
    case data.GET_TASKS: {
      return {
        ...state
      };
    }

    case data.GET_TASKS_SUCCESS: {
      return {
        ...state,
        tasks: action.payload
      };
    }

    case data.GET_TASKS_FAIL: {
      return {
        ...state
      };
    }

    case data.ADD_TASK: {
      return {
        ...state,
        // tasks: [...state.tasks, action.payload]
      };
    }

    default: {
      return state;
    }
  }
}

export const getTasks = (state: State) => state.tasks;
