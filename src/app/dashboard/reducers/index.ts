import * as fromRoot from '../../reducers';
import * as fromData from './data';
import * as fromLayout from './layout';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export interface DashboardState {
  data: fromData.State;
  layout: fromLayout.State;
}

export interface State extends fromRoot.State {
  dashboard: DashboardState;
}

export const reducers = {
  data: fromData.reducer,
  layout: fromLayout.reducer
};

export const selectDashboardState = createFeatureSelector<DashboardState>('dashboard');

export const selectDashboardDataState = createSelector(
    selectDashboardState,
  (state: DashboardState) => state.data
);

export const getTasks = createSelector(
  selectDashboardDataState,
  fromData.getTasks
);

export const selectDashboardLayoutState = createSelector(
  selectDashboardState,
  (state: DashboardState) => state.layout
);

export const getToolbarOptions = createSelector(
  selectDashboardLayoutState,
  fromLayout.getToolbarOptions
);
