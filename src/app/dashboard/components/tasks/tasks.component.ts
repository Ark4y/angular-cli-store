import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { Task } from '../../models/task.model';

import { Store } from '@ngrx/store';
import * as fromDashboard from '../../reducers';
import * as layout from '../../actions/layout';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  tasks$: Observable<Task[]>;

  constructor(
    private route: ActivatedRoute,
    private store: Store<fromDashboard.State>
  ) {
    this.tasks$ = store.select(fromDashboard.getTasks);
  }

  ngOnInit() {
    this.route.data.subscribe(
      (value) => {
        this.store.dispatch(new layout.SetToolbarOptions(value.toolbarOptions));
      }
    );
  }

}
