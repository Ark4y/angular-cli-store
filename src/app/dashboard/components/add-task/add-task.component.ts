import { ChangeDetectorRef, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Task } from '../../models/task.model';
import { ActivatedRoute, Router } from '@angular/router';

import { Store } from '@ngrx/store';
import * as fromDashboard from '../../reducers';
import * as data from '../../actions/data';
import * as layout from '../../actions/layout';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss']
})
export class AddTaskComponent implements OnInit {

  @Output() newTaskAdded = new EventEmitter<Task>();

  src: any;

  task: Task = {
    title: '',
    subtitle: '',
    picture: '',
    content: ''
  };

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<fromDashboard.State>
  ) { }

  ngOnInit() {
    this.route.data.subscribe(
      (value) => {
        this.store.dispatch(new layout.SetToolbarOptions(value.toolbarOptions));
      }
    );
  }

  addNewTask() {
    this.store.dispatch(new data.AddTask(this.task));
    // this.router.navigate(['dashboard']).then(
    //   () => {
    //     console.log('ROUTER: Navigated to dashboard.');
    //   }
    // );
  }

  fileChange(event: any) {
    this.readFiles(event.target.files);
  }

  readFile(file, reader, callback) {
    reader.onload = () => {
      callback(reader.result);
      this.task.picture = reader.result;
    };

    reader.readAsDataURL(file);
  }

  readFiles(files, index = 0) {
    const reader = new FileReader();

    if (index in files) {
      this.readFile(files[index], reader, (result) => {
        const img = document.createElement('img');
        img.src = result;
        this.src = result;

        this.readFiles(files, index + 1);
      });
    } else {
      this.changeDetectorRef.detectChanges();
    }
  }
}
