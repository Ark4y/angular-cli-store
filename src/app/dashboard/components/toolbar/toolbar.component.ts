import { Component, OnInit } from '@angular/core';

import { ToolbarOptions } from '../../models/toolbar.model';
import { Store } from '@ngrx/store';
import * as fromDashboard from '../../reducers';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  public toolbarOptions: ToolbarOptions = {
    buttons: []
  };

  constructor(
    private store: Store<fromDashboard.State>,
  ) { }

  ngOnInit() {
    this.store.select(fromDashboard.getToolbarOptions).subscribe(
      (options) => {
        this.toolbarOptions = options;
      }
    );
  }

}
