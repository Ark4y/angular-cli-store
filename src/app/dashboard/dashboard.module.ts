import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { MaterialModule } from '../material/material.module';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { DashboardPageComponent } from './containers/dashboard-page/dashboard-page.component';
import { DashboardEffects } from './effects/dashboard.effects';
import { reducers } from './reducers/index';
import { TasksComponent } from './components/tasks/tasks.component';
import { AddTaskComponent } from './components/add-task/add-task.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    RouterModule.forChild([
      {
        path: '',
        component: DashboardPageComponent,
        children: [
          {
            path: '',
            component: TasksComponent,
            data: {
              toolbarOptions: {
                buttons: [
                  {
                    title: 'Add task',
                    route: './add-task'
                  }
                ]
              }
            }
          },
          {
            path: 'add-task',
            component: AddTaskComponent,
            data: {
              toolbarOptions: {
                buttons: [
                  {
                    title: 'Cancel',
                    route: '/dashboard'
                  }
                ]
              }
            }
          }
        ]
      },
    ]),
    StoreModule.forFeature('dashboard', reducers),
    EffectsModule.forFeature([DashboardEffects])
  ],
  declarations: [
    DashboardPageComponent,
    TasksComponent,
    AddTaskComponent,
    ToolbarComponent
  ]
})
export class DashboardModule { }
