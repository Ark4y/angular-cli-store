import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lobby-page',
  templateUrl: './lobby-page.component.html',
  styleUrls: ['./lobby-page.component.scss']
})
export class LobbyPageComponent implements OnInit {

  public features: any[];

  constructor() {
    this.features = [
      {
        title: 'Weather',
        subtitle: 'Based on api.openweathermap.org',
        content: `Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                  Dicta error repudiandae deleniti repellendus aliquid tempora 
                  amet architecto delectus, quae perspiciatis!`,
        link: '/weather'
      },
      {
        title: 'Dashnoard',
        subtitle: 'Simple feature that allows user to create tasks',
        content: `Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                  Dicta error repudiandae deleniti repellendus aliquid tempora 
                  amet architecto delectus, quae perspiciatis!`,
        link: '/dashboard'
      }
    ];
  }

  ngOnInit() {
  }

}
