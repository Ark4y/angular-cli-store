import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material/material.module';
import { LobbyPageComponent } from './containers/lobby-page/lobby-page.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild([
      { path: '', component: LobbyPageComponent }
    ]),
  ],
  declarations: [
    LobbyPageComponent,
  ],
  providers: []
})
export class LobbyModule { }
