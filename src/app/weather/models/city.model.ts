export interface City {
  base?: string;
  clouds?: {
    all: string;
  };
  cod?: number;
  coord?: {
    lon: number;
    lat: number;
  };
  dt?: number;
  id?: number;
  main?: {
    humidity: number;
    pressure: number;
    temp: number;
    temp_max: number;
    temp_min: number;
  };
  name?: string;
  sys?: {
    country: string;
    id: number;
    message: number;
    sunrise: number;
    sunset: number;
    type: number;
  };
  visibility?: number;
  weather?: {
    description: string;
    icon: string;
    id: number;
    main: string;
  }[];
  wind?: {
    deg: number;
    speed: number;
  };
}
