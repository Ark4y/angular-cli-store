import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs/observable/of';

import * as WeatherActions from '../actions/data';

@Injectable()
export class WeatherEffects {

  private apiUrl = 'http://api.openweathermap.org'; /* TODO: Move to config */
  private apiKey = 'a51ad67987118dc3d44be7f421900dac'; /* TODO: Move to config */

  private url = {
    getWeatherByCityName: (cityName) => {
      return `${this.apiUrl}/data/2.5/weather?q=${cityName}&appid=${this.apiKey}`;
    }
  };

  constructor(
    private http: HttpClient,
    private actions$: Actions
  ) {
    /* -- */
  }

  @Effect() getWeatherDataByCityName$: Observable<Action> = this.actions$.ofType(WeatherActions.GET_DATA_BY_CITY_NAME)
    .mergeMap((action: WeatherActions.GetDataByCityName) => this.http.get(this.url.getWeatherByCityName(action.payload)))
    .map(data => ({type: WeatherActions.GET_DATA_BY_CITY_NAME_SUCCESS, payload: data}))
    .catch(() => of({type: WeatherActions.GET_DATA_BY_CITY_NAME_FAIL}));
}
