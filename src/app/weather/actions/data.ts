import { Action } from '@ngrx/store';
import { City } from '../models/city.model';

export const GET_DATA_BY_CITY_NAME = '[Weather / Data] Get weather data by city name';
export const GET_DATA_BY_CITY_NAME_SUCCESS = '[Weather / Data] Get weather data by city name success';
export const GET_DATA_BY_CITY_NAME_FAIL = '[Weather / Data] Get weather data by city name fail';

export class GetDataByCityName implements Action {
  readonly type = GET_DATA_BY_CITY_NAME;

  constructor(public payload: any) { }
}

export class GetDataByCityNameSuccess implements Action {
  readonly type = GET_DATA_BY_CITY_NAME_SUCCESS;

  constructor(public payload: City) { }
}

export class GetDataByCityNameFail implements Action {
  readonly type = GET_DATA_BY_CITY_NAME_FAIL;
}

export type Actions
  = GetDataByCityName
  | GetDataByCityNameSuccess
  | GetDataByCityNameFail;
