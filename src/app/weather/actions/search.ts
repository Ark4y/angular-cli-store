import { Action } from '@ngrx/store';

export const SEARCH_QUERY_CHANGE = '[Weather / Search] Search query change';

export class SearchQueryChange implements Action {
  readonly type = SEARCH_QUERY_CHANGE;

  constructor(public payload: any) { }
}

export type Actions
  = SearchQueryChange;
