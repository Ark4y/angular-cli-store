import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { City } from '../../models/city.model';
import { Observable } from 'rxjs/Observable';
import * as fromWeather from '../../reducers';
import * as weather from '../../actions/data';
import * as search from '../../actions/search';

@Component({
  selector: 'app-weather-page',
  templateUrl: './weather-page.component.html',
  styleUrls: ['./weather-page.component.scss']
})
export class WeatherPageComponent implements OnInit {

  city$: Observable<City>;
  query$: Observable<string>;

  constructor(
    private store: Store<fromWeather.State>
  ) {
    this.city$ = store.select(fromWeather.getCity);
    this.query$ = store.select(fromWeather.getSearchQuery);
  }

  ngOnInit() {
    /* -- */
  }

  public onSearchQueryChange(query: string) {
    this.store.dispatch(new search.SearchQueryChange(query));
  }

  public onSearchButtonClick(query: string) {
    this.store.dispatch(new weather.GetDataByCityName(query));
  }
}
