import { Component, Input, OnInit } from '@angular/core';
import { City } from '../../models/city.model';

@Component({
  selector: 'app-city-card',
  templateUrl: './city-card.component.html',
  styleUrls: ['./city-card.component.scss']
})
export class CityCardComponent implements OnInit {

  @Input() city: City = {};

  constructor() { }

  ngOnInit() {
    /* -- */
  }
}
