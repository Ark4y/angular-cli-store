import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {
  @Input() query = '';
  @Output() queryChange = new EventEmitter<string>();
  @Output() searchButtonClick = new EventEmitter<string>();
}
