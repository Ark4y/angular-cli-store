import * as search from '../actions/search';

export interface State {
  query: string;
}

export const initialState: State = {
  query: ''
};

export function reducer(state = initialState, action: search.Actions): State {
  switch (action.type) {
    case search.SEARCH_QUERY_CHANGE: {
      return {
        ...state,
        query: action.payload
      };
    }

    default: {
      return state;
    }
  }
}

export const getSearchQuery = (state: State) => state.query;
