import * as fromData from './data';
import * as fromSearch from './search';
import * as fromRoot from '../../reducers';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export interface WeatherState {
  data: fromData.State;
  search: fromSearch.State;
}

export interface State extends fromRoot.State {
  weather: WeatherState;
}

export const reducers = {
  data: fromData.reducer,
  search: fromSearch.reducer
};

export const selectWeatherState = createFeatureSelector<WeatherState>('weather');

export const selectWeatherDataState = createSelector(
  selectWeatherState,
  (state: WeatherState) => state.data
);

export const getCity = createSelector(
  selectWeatherDataState,
  fromData.getCity
);

export const selectWeatherSearchState = createSelector(
  selectWeatherState,
  (state: WeatherState) => state.search
);

export const getSearchQuery = createSelector(
  selectWeatherSearchState,
  fromSearch.getSearchQuery
);
