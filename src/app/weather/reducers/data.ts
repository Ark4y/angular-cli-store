import { City } from '../models/city.model';
import * as weather from '../actions/data';

export interface State {
  city: City;
}

export const initialState: State = {
  city: null
};

export function reducer(state = initialState, action: weather.Actions): State {
  switch (action.type) {
    case weather.GET_DATA_BY_CITY_NAME: {
      return {
        ...state
      };
    }

    case weather.GET_DATA_BY_CITY_NAME_SUCCESS: {
      return {
        ...state,
        city: action.payload
      };
    }

    case weather.GET_DATA_BY_CITY_NAME_FAIL: {
      return {
        ...state
      };
    }

    default: {
      return state;
    }
  }
}

export const getCity = (state: State) => state.city;
