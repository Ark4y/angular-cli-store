import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { WeatherPageComponent } from './containers/weather-page/weather-page.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { WeatherEffects } from './effects/weather.effects';
import { reducers } from './reducers/index';
import { SearchComponent } from './components/search/search.component';
import { CityCardComponent } from './components/city-card/city-card.component';
import { MaterialModule } from '../material/material.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild([
      { path: '', component: WeatherPageComponent }
    ]),
    StoreModule.forFeature('weather', reducers),
    EffectsModule.forFeature([WeatherEffects])
  ],
  declarations: [
    WeatherPageComponent,
    SearchComponent,
    CityCardComponent
  ],
  providers: []
})
export class WeatherModule { }
