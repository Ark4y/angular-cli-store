import * as layout from '../actions/layout';

export interface State {
  showSpinner: boolean;
}

const initialState: State = {
  showSpinner: false
};

export function reducer(state = initialState, action: layout.Actions): State {
  switch (action.type) {
    case layout.HIDE_SPINNER:
      return {
        showSpinner: false
      };

    case layout.SHOW_SPINNER:
      return {
        showSpinner: true
      };

    default:
      return state;
  }
}

export const getShowSpinner = (state: State) => state.showSpinner;
