import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromRoot from '../../../reducers';
import * as layout from '../../actions/layout';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  showSpinner$: Observable<boolean>;

  constructor(private store: Store<fromRoot.State>) {
    this.showSpinner$ = this.store.select(fromRoot.getShowSpinner);
  }

  hideSpinner() {
    this.store.dispatch(new layout.HideSpinner());
  }

  showSpinner() {
    this.store.dispatch(new layout.ShowSpinner());
  }
}
