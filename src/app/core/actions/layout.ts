import { Action } from '@ngrx/store';

export const SHOW_SPINNER = '[Layout] Show spinner';
export const HIDE_SPINNER = '[Layout] Hide spinner';

export class ShowSpinner implements Action {
  readonly type = SHOW_SPINNER;
}

export class HideSpinner implements Action {
  readonly type = HIDE_SPINNER;
}

export type Actions = ShowSpinner | HideSpinner;
