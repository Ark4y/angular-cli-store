import { Routes } from '@angular/router';

export const routes: Routes = [
  { path: '', redirectTo: '/lobby', pathMatch: 'full' },
  {
    path: 'lobby',
    loadChildren: './lobby/lobby.module#LobbyModule',
    // canActivate: [AuthGuard]
  },
  {
    path: 'dashboard',
    loadChildren: './dashboard/dashboard.module#DashboardModule',
    // canActivate: [AuthGuard]
  },
  {
    path: 'weather',
    loadChildren: './weather/weather.module#WeatherModule',
    // canActivate: [AuthGuard]
  },
  // { path: '**', component: NotFoundPageComponent }
];
