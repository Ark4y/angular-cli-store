var express = require('express');
var router = express.Router();

var data = {
  tasks: []
};

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/tasks', function(req, res, next) {
  res.send(data.tasks);
});

router.post('/add-task', function(req, res, next) {
  data.tasks.push(req.body);
  res.send({
    success: true
  });
});

module.exports = router;
